import Head from "next/head";
import Button from "react-bootstrap/Button";
import Image from "next/image";
import Link from "next/link";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import { useEffect, useState } from "react";

export default function Home() {
  const [orders, setOrders] = useState([]);
  const [service, setService] = useState("");
  const [execution_date, setExecutionDate] = useState("");
  const [scheduling_date, setSchedulingDate] = useState("");
  const [plate, setPlate] = useState("");

  function addOrder() {;

    let order = {
      plate: plate,
      service: service,
      scheduling_date: scheduling_date,
      execution_date: execution_date,
    };
    const newOrders=[...orders, order]
    
    localStorage.setItem('orders', JSON.stringify(newOrders));

    setOrders(newOrders);

    
  }

  function cleanOrder() {
    console.log("cleanorder");
    let test = localStorage.getItem('orders');
    console.log(test);
  }

  function deleteOrder(index) {
    const newOrders = orders;
    newOrders.splice(index,1)
    
    localStorage.setItem('orders', JSON.stringify(newOrders));

    setOrders([...newOrders]);
  }

  function finishOrder(index) {
    const newOrders = orders;
    newOrders[index].execution_date = (new Date).toLocaleDateString("pt-BR");
    
    localStorage.setItem('orders', JSON.stringify(newOrders));

    setOrders([...newOrders]);
  }


    useEffect(()=>{
      const initialOrders = localStorage.getItem('orders')
  if (initialOrders)
    setOrders(JSON.parse(initialOrders))
    }, [])

  return (
    <>
      <Head>
        <title>CRM - EasyCarros</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Container>
        <Card id="card1">
          <Card.Body>
            <Card.Title id="title">Nova ordem de serviço</Card.Title>
            <Card.Subtitle id="subtitle" className="mb-2 text-muted">
              Os campos com * são obrigatórios
            </Card.Subtitle>
            <Form>
              <Row>
                <Col>
                  <Form.Label id="label1">Serviço*</Form.Label>
                  <Form.Control
                    placeholder="Lavagem Ecologica COVID-19"
                    onChange={(e) => {
                      setService(e.target.value);
                    }}
                  />
                </Col>

                <Col>
                  <Form.Label id="label2">Data de agendamento*</Form.Label>
                  <Form.Control type="date"
                    placeholder="data"
                    onChange={(e) => {
                      setSchedulingDate(e.target.value);
                    }}
                  />
                </Col>
                <Col>
                  <Form.Label>&nbsp;</Form.Label>
                  <Form.Control
                    placeholder="Placa*"
                    onChange={(e) => {
                      setPlate(e.target.value);
                    }}
                  />
                </Col>
              </Row>
            </Form>
            <Link href="/"><Button
              variant="outline-primary"
              className="button"
              id="cancelar"
              onClick={cleanOrder}
            >
              Cancelar
            </Button></Link>
            <Button
              variant="primary"
              className="button"
              id="adicionar"
              onClick={() => {
                addOrder();
              }}
            >
              Adicionar
            </Button>
          </Card.Body>
        </Card>

        <Table striped bordered hover id="table">
          <thead id="thead">
            <tr>
              <th>Serviço</th>
              <th>Data de Execução</th>
              <th>Data de Agendamento</th>
              <th>Placa</th>
              <th> </th>
            </tr>
          </thead>
          <tbody id="tbody">
            {orders.map((order, index) => (
              <tr>
                <td>{order.service}</td>
                <td>{order.execution_date}</td>
                <td>{(new Date(order.scheduling_date.slice(0,4),order.scheduling_date.slice(5,7)-1,order.scheduling_date.slice(8,10))).toLocaleDateString("pt-BR")}</td>
                <td>{order.plate}</td>
                <Button variant="outline-danger" id="excluir" onClick={()=>{
                  
                  deleteOrder(index);}} >
                  Excluir
                </Button>
                
                {! order.execution_date && <Button variant="outline-success" id="finalizar" onClick={()=>{

                  finishOrder(index);}}>
                  Finalizar
                </Button>}
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </>
  );
}
